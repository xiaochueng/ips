#coding=utf-8
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import datetime
from selenium import webdriver
from common.logger import Logger

logger = Logger(logger='basepage').getlog()

class BasePage:
    def __init__(self,driver):
        self.driver = driver

    #等待元素可见
    def wait_eleVisible(self,locator,wait_time=30,poll_frequency=0.5,doc=''):
        '''
        :param locator:元素定位，元组形式（元素定位类型，元素定位方式）
        :param time:
        :param poll_frequency:
        :param doc: 模块名_页面名称_操作名称
        :return:None
        '''
        logger.info("{1} 等待元素{0}可见".format(locator,doc))
        try:
            #开始等待时间
            start = datetime.datetime.now()
            WebDriverWait(self.driver, wait_time,poll_frequency).until(EC.visibility_of_element_located(locator))
            end = datetime.datetime.now()
            #等待时长
            wait_time = (end-start).seconds
            logger.info("等待时长：{}".format(wait_time))
        except:
            logger.exception("等待元素可见失败！！！")
            #截图
            self.save_web_screenshot(doc)
            raise



    #元素是否存在
    def wait_elePresence(self,locator,time=30,poll_frequency=0.5,doc=''):
        logger.info("{1} 判断元素{0}是否存在".format(locator,doc))
        try:
            #开始等待时间
            start = datetime.datetime.now()
            WebDriverWait(self.driver, time,poll_frequency).until(EC.visibility_of_element_located(locator))
            end = datetime.datetime.now()
            #等待时长
            wait_time = (end-start).seconds
            logger.info("元素存在，等待时长：{}".format(wait_time))
            return True
        except:
            logger.exception("元素不存在，请检查元素")
            #截图
            self.save_web_screenshot(doc)
            return False

    #查找元素
    def get_element(self,locator,doc=""):
        logger.info("{0} 查找元素：{1}".format(doc,locator))
        try:
            return self.driver.find_element(*locator)
        except:
            logger.exception("查找元素失败！！！")
            #截图
            self.save_web_screenshot(doc)
            raise

    #多层级查找元素
    def get_multi_element(self,locator,locator1,doc=""):
        '''
        :param locator: 父级元素定位
        :param locator1: 子级元素定位
        :param doc: 模块名_页面名称_操作名称
        :return:
        '''
        logger.info("{2} 多层查找元素：{0},{1}".format(locator,locator1,doc))
        try:
            return self.driver.find_element(*locator).find_element(*locator1)
        except:
            logger.exception("查找元素失败！！！")
            #截图
            self.save_web_screenshot(doc)
            raise


    #点击操作
    def click_element(self,locator,doc=""):
        #找元素
        ele = self.get_element(locator,doc)
        logger.info("{0} 点击元素:{1}".format(doc,locator))
        try:
            ele.click()
        except:
            logger.exception("元素点击操作失败！！！")
            #截图
            self.save_web_screenshot(doc)
            raise

    #多层级元素点击操作
    def click_mutil_element(self,locator,locator1,doc=""):
        '''
        :param locator: 父级元素定位
        :param locator1: 子级元素定位
        :param doc: 模块名_页面名称_操作名称
        :return:
        '''
        #多层级找元素
        ele = self.get_multi_element(locator,locator1,doc)
        logger.info("{0} 多层点击元素:{1}".format(doc,locator1))
        try:
            ele.click()
        except:
            logger.exception("元素点击操作失败！！！")
            #截图
            self.save_web_screenshot(doc)
            raise


    #输入操作
    def input_text(self,locator,text,doc=""):
        #找元素
        ele = self.get_element(locator,doc)
        logger.info("{0} 输入元素:{1}".format(doc,locator))
        try:
            ele.send_keys(text)
        except:
            logger.exception("元素输入操作失败！！！")
            #截图
            self.save_web_screenshot(doc)
            raise

    #多层级元素输入操作
    def input_mutil_text(self,locator,locator1,text,doc=""):
        '''
        :param locator: 父级元素定位
        :param locator1: 子级元素定位
        :param text: 元素操作输入的内容
        :param doc: 模块名_页面名称_操作名称
        :return:
        '''
        #多层级找元素
        ele = self.get_multi_element(locator,locator1,doc)
        logger.info("{0} 多层输入元素:{1},{2}".format(doc,locator1,locator))
        try:
            ele.send_keys(text)
        except:
            logger.exception("元素输入操作失败！！！")
            #截图
            self.save_web_screenshot(doc)
            raise


    #获取元素的文本内容
    def get_text(self,locator, doc=""):
        ele = self.get_element(locator, doc)
        logger.info("{0} 获取元素文本内容:{1}".format(doc, locator))
        try:
            return ele.text
        except:
            logger.exception("获取元素文本内容失败！！！")
            #截图
            self.save_web_screenshot(doc)
            raise

    #多层级获取元素的文本内容
    def get_mutil_text(self,locator,locator1, doc=""):
        # 多层级找元素
        ele = self.get_multi_element(locator, locator1,doc)
        logger.info("{0} 获取元素文本内容:{1}".format(doc, locator1))
        try:
            return ele.text
        except:
            logger.exception("获取元素文本内容失败！！！")
            #截图
            self.save_web_screenshot(doc)
            raise


    #获取元素的属性
    def get_element_attribute(self,locator,attr, doc):
        #查找元素
        ele = self.get_element(locator, doc)
        logger.info("{0} 获取元素属性:{1}".format(doc, locator))
        try:
            return ele.get_attribute(attr)
        except:
            logger.exception("获取元素属性失败！！！")
            #截图
            self.save_web_screenshot(doc)
            raise

    #多层级获取元素的属性
    def get_mutil_element_attribute(self,locator,locator1,attr, doc):
        '''
        :param locator: 父级元素定位
        :param locator1: 子级元素定位
        :param attr: 需要获取的属性
        :param doc: 模块名_页面名称_操作名称
        :return:
        '''
        #多层级找元素
        ele = self.get_multi_element(locator,locator1, doc)
        logger.info("{0} 获取元素属性:{1}".format(doc, locator1))
        try:
            return ele.get_attribute(attr)
        except:
            logger.exception("获取元素属性失败！！！")
            #截图
            self.save_web_screenshot(doc)
            raise


    #iframe切换
    def switch_iframe(self,iframe_reference):
        pass

    #上传操作

    #截图
    def save_web_screenshot(self,doc):
        #图片名称：模块名_页面名称_操作名称_年-月-日_时分秒.png
        logger.info("case失败，截图-->")
        filename = "E:\worksapce\ips\Outputs\screenshoots"+"\\"+"{0}_{1}.png".format(doc,self.get_now_time())
        try:
            self.driver.save_screenshot(filename)
            logger.info("截屏成功，文件路径为：{0}".format(filename))
        except:
            logger.exception("截图失败")

    #获取当前时间
    def get_now_time(self):
        return datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

    #获取截屏存放路径
    # def get_screenshoots_path(self):
    #     #截屏文件存放目录，先放这
    #     filepath = 'Outputs\screenshoots'
    #     return os.path.abspath(filepath)


if __name__ == "__main__":
    driver = webdriver.Chrome()
    res = BasePage(driver)
    res.save_web_screenshot("lelemene")
    driver.close()