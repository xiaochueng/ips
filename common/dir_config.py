#coding=utf-8
import os

#
base_dir = os.path.split(os.path.split(os.path.abspath(__file__))[0])[0]

test_data_dir = os.path.join(base_dir,'test_data')

test_case_dir = os.path.join(base_dir,'test_case')

#日志存放目录
logs_dir = os.path.join(base_dir,'Outputs\logs\\')
#日志打印级别
logs_level = '1'
#截图存放路径
screenshot_dir = os.path.join(base_dir,'Outputs\screenshots')

#用例存放路径
case_path = 'E:\worksapce\ips\\test_case\\'

#报告生成路径
report_path = 'E:\worksapce\ips\Outputs\\reports\\'

