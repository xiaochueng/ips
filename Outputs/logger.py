# _*_ coding: utf-8 _*_
import logging
import os.path
import time
from tools.ReadConfig import log_level, log_path

class Logger(object):
    def __init__(self, logger):
        '''''
            指定保存日志的文件路径，日志级别，以及调用文件
            将日志存入到指定的文件中
        '''

        # 创建一个logger
        self.logger = logging.getLogger(logger)
        self.logger.setLevel(logging.DEBUG)

        # 用于写入日志文件
        rq = time.strftime('%Y%m%d%H%M', time.localtime(time.time()))
        if not os.path.exists(log_path):
            os.makedirs(log_path)
        else:
            pass
        log_name = log_path + rq + '.log'
        # print(log_name)
        fh = logging.FileHandler(log_name, encoding='utf-8')
        fh.setLevel(logging.DEBUG)

        # 用于输出到控制台
        ch = logging.StreamHandler()
        ch.setLevel(log_level)

        # 定义handler的输出格式
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        # 给logger添加handler
        self.logger.addHandler(fh)
        self.logger.addHandler(ch)

    def getlog(self):
        return self.logger
